# README #

### Project Unicorn Vehicle Chassis Control ###
(May 2014)

Exchanges data with the vehicle's Raspberry Pi using the i2c bus, controls steering servo and the motor controller in response to commands received and outputs vehicle data to the external LCD display.

### Related Repositories ###

-	Python Vehicle Controller (https://bitbucket.org/adambauman/python-unicorn)