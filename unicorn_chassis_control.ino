// Project Unicorn Vehicle Chassis Control
// (May 2014) Adam J. Bauman, Agent 1973
//
// Exchanges data with the vehicle's Raspberry Pi using the i2c bus,
// controls steering servo and the motor controller in response to 
// commands received and outputs vehicle data to the external LCD display.
//

#include<stdlib.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <Wire.h>
#include <Servo.h>

// -----------------------------------------------------------
//    Hardware Setup
// -----------------------------------------------------------
//Arduino Pro Mini PWM pins: 3, 5, 6, 9, 10, and 11

// Define the Arduino's I2C slave address
#define SLAVE_ADDRESS 0x04

// Steering servo parameters
Servo steeringServo;

int pinSteeringServo = 9;
int pinSteeringAngle = A0;

/* Old steering values pre-signal wierdness
int steeringLeft = 68;
int steeringCenter = 86;
int steeringRight = 98;
*/

// Initial testing values: 650, 804, 959
int steeringLeft = 710;
int steeringCenter = 800;
int steeringRight = 850;

// Initial testing values: 433, 471, 388
int steeringRefLeft = 433;
int steeringRefCenter = 471;
int steeringRefRight = 388;

// Propulsion motor parameters
int pinMotorEnA = 11;
int pinMotorIn1 = 12;
int pinMotorIn2 =  13;

int propMotorSpeed = 255; // 0-255

// Wheel sensor parameters
int pinWsensClk = 5;
int pinWsensData = 8;
int pinWsensPD = A1;

// Nokia 5110 LCD Pin assignments  
// pin 7 - Serial clock out (SCLK) / Pin 3 (CLK)
// pin 6 - Serial data out (DIN) / Pin 4 (DIN)
// pin 4 - Data/Command select (D/C) / Pin 5 (D/C)
// pin 3 - LCD chip select (CS) / Pin 6 (CS, GND if not connected)
// pin 2 - LCD reset (RST) / Pin 7 (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 4, 3, 2);

// -----------------------------------------------------------
//    Variable Setup
// -----------------------------------------------------------

// Set by the RPI, tells us which command to run
int commandReceived;
int commandLast;
boolean commandTrigger = false;

// System conditions
float commandVoltage = 00.0;
float propulsionVoltage = 00.0;
boolean steeringGood = true;
boolean propulsionGood = true;

// Display variables
boolean statusPageTwo;

void setup()   
{
  // Enable serial comms for debugging
  //Serial.begin(115200);
  //Serial.println("Serial Debug Initialized");

  // Initialize i2c communications  
  Wire.begin(SLAVE_ADDRESS);

  // Attach to the steering servo and center it
  steeringServo.attach(pinSteeringServo);
  steeringServo.writeMicroseconds(steeringCenter);
  
  // Initialize the LCD
  display.begin();
  display.setTextWrap(0);
  display.setContrast(60);
  display.clearDisplay();

  // Run a self test
  SelfTest();
  
  // Define how the microcontrol will react to i2c requests
  Wire.onReceive(ReceiveData);
  Wire.onRequest(SendData);
}

void loop()
{
  if (commandTrigger)
    MotionControl();
    
  delay(100);
}

// -----------------------------------------------------------
//    ReceiveData() - triggered when i2c master speaks
// -----------------------------------------------------------
void ReceiveData(int bytecount)
{
  // Flush the LCD
  display.clearDisplay();
  
  // Store our current command for comparison
  commandLast = commandReceived;
  
  // Grab the data from the RPI
  while(Wire.available())
  {
    commandReceived = Wire.read();
  } 
  
  Serial.println("i2c command received: " + commandReceived);
  
  commandTrigger = true;
}

// -----------------------------------------------------------
//    SendData() - sends data back to the i2c master
// -----------------------------------------------------------
void SendData()
{
  Wire.write(1);
}

// -----------------------------------------------------------
//    MotionControl() - the hub for all our movement
// -----------------------------------------------------------
void MotionControl()
{
  switch (commandReceived)
  {
    case 1: // Forward Left
      SteeringControl('L');
      DriveMotor(0, 300);
      break;
    case 2: // Forward
      SteeringControl('C');
      DriveMotor(0, 450);
      break;
    case 3: // Forward Right
      SteeringControl('R');
      DriveMotor(0, 300);
      break;
    case 4: // Reverse Left
      SteeringControl('L');
      DriveMotor(1, 300);
      break;
    case 5: // Reverse
      SteeringControl('C');
      DriveMotor(1, 450);
      break;
    case 6: // Reverse Right
      SteeringControl('R');
      DriveMotor(1, 300);
      break;
  }
  
  // Re-center the steering to relieve crappy rack stress
  delay(2000);
  SteeringControl('C');
 
  // Reset the command trigger
  commandTrigger = false;
}

// -----------------------------------------------------------
//    SteeringControl() - moves the steering servo
// -----------------------------------------------------------
void SteeringControl(char steeringDirection)
{
  int referenceAngle;
  int steeringAngle;
  
  switch (steeringDirection)
  {
    case 'C':
      Serial.println("Center triggered");
      steeringServo.writeMicroseconds(steeringCenter);
      referenceAngle = steeringRefCenter;
      break;
    case 'L':
      Serial.println("Left triggered");
      steeringServo.writeMicroseconds(steeringLeft);
      referenceAngle = steeringRefLeft;
      break;
    case 'R':
      Serial.println("Right triggered");
      steeringServo.writeMicroseconds(steeringRight);
      referenceAngle = steeringRefRight;
      break;
  }

  // Give the servo a chance to move
  delay(500);
 
  steeringAngle = analogRead(pinSteeringAngle); 
  
  // Read the servo's current angle and check that it didn't get stuck
  if (steeringAngle <= (referenceAngle - 5) && steeringAngle >= referenceAngle + 5)
  {
    steeringGood = true;
  } else {
    steeringGood = false;
  }

  Serial.println(steeringAngle);
}

// -----------------------------------------------------------
//    DriveMotor() - fires up the propulsion motor
// -----------------------------------------------------------
void DriveMotor(int reverse, int duration)
{
  // Fire up the propulsion motor
  digitalWrite(pinMotorIn1, reverse);
  digitalWrite(pinMotorIn2, !reverse);
  analogWrite(pinMotorEnA, propMotorSpeed);
 
  delay(duration);
 
  // Stop the motor
  analogWrite(pinMotorEnA, 0);
 
  // Check that we moved (coming soon!)
}

// -----------------------------------------------------------
//    DisplayStatus() - shows ongoing system status
// -----------------------------------------------------------
void DisplayStatusOne()
{
  // Convert voltage floats to string
  char ctrlBattDisplay[5]; 
  char propBattDisplay[5]; 
  dtostrf(commandVoltage, 4, 3, ctrlBattDisplay);
  dtostrf(propulsionVoltage, 4, 3, propBattDisplay);
  
  // Flush the LCD
  display.clearDisplay();
  display.println("SYSTEMS STATUS");
  display.println("--------------");
  display.println("ctrlbatt " + String(ctrlBattDisplay) + "v");
  display.println("propbatt " + String(propBattDisplay) + "v");
  
  // Display steering status
  if (steeringGood)
  {
    display.println("steering    ok");
  } else {
    display.println("Steering   err");
  }
  
  // Display propulsion status
  if (propulsionGood)
  {
    display.println("propulsion  ok");
  } else {
    display.println("propulsion err");
  }
    
  display.display();
}

void DisplayStatusTwo()
{
   
}

// -----------------------------------------------------------
//    SelfTest() - runs a self test on all systems
// -----------------------------------------------------------
void SelfTest()
{
  Serial.println("Starting self test");
  boolean testFailed = false;
  boolean neverGonnaGiveYouUp = false;
  
  // Flush the LCD
  display.clearDisplay();
  display.println("  SELF  TEST  ");
  display.println("--------------");
  display.display();
   
  // Test steering
  SteeringControl('L');
  delay(800);
  SteeringControl('R');
  delay(800);
  SteeringControl('C');
  
  if (steeringGood)
  {
    display.println("steering    ok");
  } else {
    display.println("Steering   err");
    testFailed = true;
  }
  display.display();

  // Test propulsion
  delay(1000);
  DriveMotor(0, 250);
  delay(1000);
  DriveMotor(1, 250);
  
  if (propulsionGood)
  {
    display.println("propulsion  ok");
  } else {
    display.println("propulsion err");
    testFailed = true;
  }
  display.println("");
  display.display();
  
  if (testFailed)
  {
    display.println("SYS FAIL HALT");
    display.display();
    
    // Lock the microcontroller into a loop until reset
   /*
    while (neverGonnaGiveYouUp = false)
    {
      delay(1000);
    }
    */
  } else {
    display.println("ALL SYSTEMS GO"); 
  }
  
  // A little delay for effect
  delay(1000);
  display.display();
  
}

// -----------------------------------------------------------
//    AllStop() - hit the brakes, loop until controller reset
// -----------------------------------------------------------
void AllStop()
{
  analogWrite(pinMotorEnA, 0);
  SteeringControl('C');
  Serial.println("**** ALL STOP ****");
}

// -----------------------------------------------------------
//    Uptime() - uptime counter
// -----------------------------------------------------------  
String Uptime()
{
  // Math variables
  long days=0;
  long hours=0;
  long mins=0;
  long secs=0;
  
  // Return variable
  String returnString;
  
  // Cut running millis to time units
  secs = millis() / 1000;
  mins = secs / 60;
  hours = mins / 60;
  days = hours / 24;
  secs = secs - (mins * 60);
  mins = mins - (hours * 60);
  hours = hours - (days * 24);
  
  //Display results
  returnString.concat(hours);
  returnString.concat("h");
  returnString.concat(mins);
  returnString.concat("m");
  returnString.concat(secs);
  returnString.concat("s");
  
  Serial.println(returnString);
  
  return returnString;
  
}

